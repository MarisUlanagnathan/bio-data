import React, { useState,useEffect } from "react";
import "./App.css";
import Select from "react-select";
import {typeOfLanguage} from './LanguageStrings/language'

const options = [
  { value: "tamil", label: "தமிழ்" },
  { value: "english", label: "English" },
];
const colourStyles = {
  control: (styles) => ({
    ...styles,
    backgroundColor: "white",
    width: "300px",
  }),
};

function App() {
  const [value, setValue] = useState();
  const [languageValue,setLanguageValue] = useState({})
  useEffect(() => {
    setLanguageValue(typeOfLanguage["tamil"])
  },[]) 
  const handleChange = (selectedOption) => {
    setValue(selectedOption);
    console.log(selectedOption,"PPPPP")
    if (typeof window !== "undefined") {
      if (window.fbq != null) { 

        window.fbq('trackCustom', 'SelectLanguage', {answer:selectedOption.value});

      }
    }
    
    // fbq('track', 'handlelanguage', selectedOption)
    setLanguageValue(typeOfLanguage[selectedOption.value])
  };
  return (
    <>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <div
          style={{
            height: "30px",
            backgroundColor: "yellow",
            width: 38,
            borderBottomRightRadius: 5,
          }}
        ></div>
        <div
          style={{
            height: "30px",
            backgroundColor: "yellow",
            width: 38,
            borderBottomLeftRadius: 5,
          }}
        ></div>
      </div>
      <div className="App">
        <span>உ</span>
        <span>{languageValue["god"]}</span>
        <span>{languageValue["bioHeader"]}</span>
        <br />
        <div className="drop">
          {/* <span>hi</span> */}
          <Select
            value={value}
            placeholder="மொழியை மாற்றுக"
            onChange={handleChange}
            styles={colourStyles}
            options={options}
          />{" "}
        </div>
        <br />
        <table className="one">
          <tr>
            <td>{languageValue["nameTitle"]}</td>
            <td>{languageValue["name"]}</td>
          </tr>
          <tr>
            <td>{languageValue["dob"]}</td>
            <td>17-12-1996</td>
          </tr>
          <tr>
            <td>{languageValue["age"]}</td>
            <td>25</td>
          </tr>
          <tr>
            <td>{languageValue["height"]}</td>
            <td>5.8</td>
          </tr>


          <tr>
            <td>{languageValue["natchathiramHeader"]}</td>
            <td>{languageValue["natchathiram"]}</td>
          </tr>
          <tr>
            <td>{languageValue["rasiHeader"]}</td>
            <td>{languageValue["rasi"]}</td>
          </tr>


          
          <tr>
            <td>{languageValue["education"]}</td>
            <td>B.E ECE</td>
          </tr>
          <tr>
            <td>{languageValue["salary"]}</td>
            <td>₹ 27,000</td>
          </tr>
          <tr>
            <td>{languageValue["jobTitle"]}</td>
            <td>{languageValue["jobTitleValue"]}</td>
          </tr>
          <tr>
            <td>{languageValue["companyName"]}</td>
            <td>Vknowlabs Pvt.Ltd</td>
          </tr>
          <tr>
            <td>{languageValue['fatherName']}</td>
            <td>{languageValue["fName"]}</td>
          </tr>
          <tr>
            <td>{languageValue["motherName"]}</td>
            <td>{languageValue["mName"]}</td>
          </tr>
          <tr>
            <td>{languageValue["contactNo"]}</td>
            <td>8608194115 / 7449065289 </td>
          </tr>
          <tr>
            <td>{languageValue["religion"]}</td>
            <td>{languageValue["reliValue"]}</td>
          </tr>
          <tr>
            <td>{languageValue["siblings"]}</td>
            <td>-</td>
          </tr>
        </table>
      </div>
      <div className="rasi">
        <table className="first-table">
          <tr>
            <td>கேது</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>
              <span>சந்</span>
              <br />
              <span style={{ textDecoration: "none" }}>சனி</span>
            </td>
            <td style={{ border: "none" }}></td>

            <td style={{ border: "none" }}></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td style={{ border: "none" }}>
              {" "}
              <span>இராசி</span>
            </td>
            <td style={{ border: "none" }}></td>
            <td>
              <span>செவ்</span>
            </td>
          </tr>
          <tr>
            <td>
              <span>சூாி,புத</span>
              <br />
              <span style={{ textDecoration: "none" }}>குரு</span>
              <br />
            </td>
            <td>
              <span>ல/சுக்</span>
            </td>
            <td></td>
            <td>
              <span>ராகு</span>
            </td>
          </tr>
        </table>

        <table className="second-table">
          <tr>
            <td>ராகு</td>
            <td>
              <span>சூாி</span>
            </td>
            <td>சந்</td>
            <td>
              <span>புத</span>
              <br />
              <span style={{ textDecoration: "none" }}>சனி</span>
            </td>
          </tr>
          <tr>
            <td>
              <span>ல</span>
            </td>
            <td style={{ border: "none" }}></td>

            <td style={{ border: "none" }}></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td style={{ border: "none" }}>
              {" "}
              <span>அம்சம்</span>
            </td>
            <td style={{ border: "none" }}></td>
            <td>
              <span>சுக்</span>
            </td>
          </tr>
          <tr>
            <td>
              <span>செவ்</span>

              <br />
              <span style={{ textDecoration: "none" }}>குரு</span>
              <br />
            </td>
            <td></td>
            <td></td>
            <td>
              <span>கேது</span>
            </td>
          </tr>
        </table>
      </div>
    </>
  );
}

export default App;
